#############################################################
#
# gettext
#
#############################################################
GETTEXT_VERSION:=0.17
GETTEXT_SOURCE:=gettext-$(GETTEXT_VERSION).tar.gz
GETTEXT_SITE:=$(BR2_GNU_MIRROR)/gettext
GETTEXT_DIR:=$(BUILD_DIR)/gettext-$(GETTEXT_VERSION)
GETTEXT_CAT:=$(ZCAT)
GETTEXT_BINARY:=gettext-runtime/src/gettext
GETTEXT_TARGET_BINARY:=usr/bin/gettext

ifeq ($(BR2_PACKAGE_GETTEXT_STATIC),y)
LIBINTL_TARGET_BINARY:=usr/lib/libintl.a
else
LIBINTL_TARGET_BINARY:=usr/lib/libintl.so
endif

$(DL_DIR)/$(GETTEXT_SOURCE):
	 $(WGET) -P $(DL_DIR) $(GETTEXT_SITE)/$(GETTEXT_SOURCE)

gettext-source: $(DL_DIR)/$(GETTEXT_SOURCE)

$(GETTEXT_DIR)/.unpacked: $(DL_DIR)/$(GETTEXT_SOURCE)
	$(GETTEXT_CAT) $(DL_DIR)/$(GETTEXT_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(GETTEXT_DIR) package/gettext/ gettext\*.patch
	#$(CONFIG_UPDATE) $(@D)
	$(CONFIG_UPDATE) $(GETTEXT_DIR)/build-aux
	touch $@

ifeq ($(BR2_TOOLCHAIN_EXTERNAL),y)
IGNORE_EXTERNAL_GETTEXT:=--with-included-gettext
endif

$(GETTEXT_DIR)/.configured: $(GETTEXT_DIR)/.unpacked
	(cd $(GETTEXT_DIR); rm -rf config.cache; \
		$(AUTO_CONFIGURE_TARGET) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--disable-libasprintf \
		--enable-shared \
		$(IGNORE_EXTERNAL_GETTEXT) \
		$(OPENMP) \
	)
	touch $@

$(GETTEXT_DIR)/$(GETTEXT_BINARY): $(GETTEXT_DIR)/.configured
	$(MAKE) -C $(GETTEXT_DIR)
	touch -c $@

$(STAGING_DIR)/$(GETTEXT_TARGET_BINARY): $(GETTEXT_DIR)/$(GETTEXT_BINARY)
	$(MAKE) DESTDIR=$(STAGING_DIR) -C $(GETTEXT_DIR) install
	$(SED) 's,/lib/,$(STAGING_DIR)/usr/lib/,g' $(STAGING_DIR)/usr/lib/libgettextlib.la
	$(SED) 's,/lib/,$(STAGING_DIR)/usr/lib/,g' $(STAGING_DIR)/usr/lib/libgettextpo.la
	$(SED) 's,/lib/,$(STAGING_DIR)/usr/lib/,g' $(STAGING_DIR)/usr/lib/libgettextsrc.la
	$(SED) "s,^libdir=.*,libdir=\'$(STAGING_DIR)/usr/lib\',g" $(STAGING_DIR)/usr/lib/libgettextlib.la
	$(SED) "s,^libdir=.*,libdir=\'$(STAGING_DIR)/usr/lib\',g" $(STAGING_DIR)/usr/lib/libgettextpo.la
	$(SED) "s,^libdir=.*,libdir=\'$(STAGING_DIR)/usr/lib\',g" $(STAGING_DIR)/usr/lib/libgettextsrc.la
	$(SED) "s,^libdir=.*,libdir=\'$(STAGING_DIR)/usr/lib\',g" $(STAGING_DIR)/usr/lib/libintl.la
	rm -f $(addprefix $(STAGING_DIR)/usr/bin/, \
		autopoint envsubst gettext.sh gettextize msg* ?gettext)
	touch -c $@

gettext: pkgconfig $(STAGING_DIR)/$(GETTEXT_TARGET_BINARY)

gettext-unpacked: $(GETTEXT_DIR)/.unpacked

gettext-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) CC=$(TARGET_CC) -C $(GETTEXT_DIR) uninstall
	-$(MAKE) -C $(GETTEXT_DIR) clean

gettext-dirclean:
	rm -rf $(GETTEXT_DIR)

#############################################################
#
# gettext on the target
#
#############################################################

gettext-target: $(GETTEXT_DIR)/$(GETTEXT_BINARY)
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(GETTEXT_DIR) install
	chmod +x $(TARGET_DIR)/usr/lib/libintl.so* # identify as needing to be stripped
ifneq ($(BR2_HAVE_INFOPAGES),y)
	rm -rf $(TARGET_DIR)/usr/info
endif
ifneq ($(BR2_HAVE_MANPAGES),y)
	rm -rf $(TARGET_DIR)/usr/man
endif
	rm -rf $(addprefix $(TARGET_DIR),/usr/share/doc \
		/usr/doc /usr/share/aclocal /usr/include/libintl.h)
	rmdir --ignore-fail-on-non-empty $(TARGET_DIR)/usr/include

$(TARGET_DIR)/usr/lib/libintl.so: $(STAGING_DIR)/$(GETTEXT_TARGET_BINARY)
	cp -dpf $(STAGING_DIR)/usr/lib/libgettext*.so* \
		$(STAGING_DIR)/usr/lib/libintl*.so* $(TARGET_DIR)/usr/lib/
	rm -f $(addprefix $(TARGET_DIR)/usr/lib/, \
		libgettext*.so*.la libintl*.so*.la)
	touch -c $@

$(TARGET_DIR)/usr/lib/libintl.a: $(STAGING_DIR)/$(GETTEXT_TARGET_BINARY)
	cp -dpf $(STAGING_DIR)/usr/lib/libgettext*.a $(TARGET_DIR)/usr/lib/
	cp -dpf $(STAGING_DIR)/usr/lib/libintl*.a $(TARGET_DIR)/usr/lib/
	touch -c $@

libintl: $(TARGET_DIR)/$(LIBINTL_TARGET_BINARY)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(BR2_PACKAGE_LIBINTL),y)
TARGETS+=libintl
endif
ifeq ($(BR2_PACKAGE_GETTEXT),y)
TARGETS+=gettext
endif
