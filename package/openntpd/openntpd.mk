#############################################################
#
# OpenNTPD
#
#############################################################
OPENNTPD_VERSION:=4.4
OPENNTPD_SOURCE:=openntpd-$(OPENNTPD_VERSION).tgz
OPENNTPD_VERSION:=3.9p1
OPENNTPD_SOURCE:=openntpd-$(OPENNTPD_VERSION).tar.gz
OPENNTPD_SITE:=ftp://ftp.openbsd.org/pub/OpenBSD/OpenNTPD
OPENNTPD_SITE:=http://ftp.eu.openbsd.org/pub/OpenBSD/OpenNTPD/
#OPENNTPD_DIR:=$(BUILD_DIR)/openntpd-$(OPENNTPD_VERSION)
OPENNTPD_BINARIES:=ntpd
OPENNTPD_CONF_OPT:= --with-builtin-arc4random

$(eval $(call AUTOTARGETS,package,openntpd))

#$(OPENNTPD_TARGET_CONFIGURE): ;
$(OPENNTPD_TARGET_BUILD):
	(cd $(@D); \
	$(TARGET_CC) $(TARGET_CC_FLAGS) $(TARGET_CFLAGS) -I$(@D) \
		-D__USE_XOPEN \
		$(if $(BR2_PREFER_IMA),$(CFLAGS_COMBINE) $(CFLAGS_WHOLE_PROGRAM)) \
		-o $(@D)/$(OPENNTPD_BINARIES) $(wildcard $(@D)/*.c $(@D)/openbsd-compat/*.c); \
	)
	touch $@

$(OPENNTPD_TARGET_INSTALL_TARGET):
	rm -f $(TARGET_DIR)/etc/ntpd.conf
	$(INSTALL) -D -m 0644 $(OPENNTPD_DIR)/ntpd.conf $(TARGET_DIR)/etc/ntpd.conf
	$(INSTALL) -m 755 $(addprefix $(OPENNTPD_DIR)/,$(OPENNTPD_BINARIES)) $(TARGET_DIR)/usr/sbin
	$(STRIPCMD) $(STRIP_STRIP_ALL) $(addprefix $(TARGET_DIR)/usr/sbin/,$(OPENNTPD_BINARIES))
	touch $@

$(OPENNTPD_TARGET_CLEAN):
	-$(MAKE) -C $(OPENNTPD_DIR) clean
	rm -f $(TARGET_DIR)/etc/ntpd.conf \
		$(wildcard $(TARGET_DIR)/usr/share/man*/ntpd*) \
		$(addprefix $(TARGET_DIR)/usr/sbin,$(OPENNTPD_BINARIES))
	touch $@

ntpd-dirclean:
	rm -rf $(OPENNTPD_DIR)

