#############################################################
#
# perl
#
#############################################################
PERL_MAJ=5
PERL_VERSION=$(PERL_MAJ).8.8
PERL_SOURCE=perl-$(PERL_VERSION).tar.bz2
PERL_CAT:=$(BZCAT)
PERL_SITE=http://www.cpan.org/src/
PERL_DIR=$(BUILD_DIR)/perl-$(PERL_VERSION)

PERL_MODS_DIR=/usr/lib/perl$(PERL_MAJ)/$(PERL_VERSION)
PERL_MODS=$(subst ",,$(BR2_PACKAGE_PERL_MODULES))
# ")
ifeq ($(BR2_PACKAGE_AUTOMAKE),y)
PERL_MODS+=File/Basename.pm Errno.pm Config.pm IO/File.pm Symbol.pm \
	SelectSaver.pm IO/Seekable.pm IO/Handle.pm IO.pm XSLoader.pm \
	DynaLoader.pm AutoLoader.pm Carp/Heavy.pm
endif

ifeq ($(BR2_ENDIAN),"BIG")
ifeq ($(KERNEL_BITS),64)
PERL_BYTEORDER:=87654321
endif
ifeq ($(KERNEL_BITS),32)
PERL_BYTEORDER:=4321
endif
endif
ifeq ($(BR2_ENDIAN),"LITTLE")
ifeq ($(KERNEL_BITS),64)
PERL_BYTEORDER:=12345678
endif
ifeq ($(KERNEL_BITS),32)
PERL_BYTEORDER:=1234
endif
endif
ifeq ($(KERNEL_BITS),32)
PERL_is_32:=y
PERL_SIZEOF_LONG:=4
PERL_SIZEOF_LONG_LONG:=8
PERL_SIZEOF_LONG_DOUBLE:=12
PERL_TYPE_i32:=long
PERL_TYPE_i64:=long long
PERL_TYPE_QUAD:=long long
PERL_QUAD_KIND:=3
PERL_UID_FORMAT:=lu
endif
ifeq ($(KERNEL_BITS),64)
PERL_SIZEOF_LONG:=8
PERL_SIZEOF_LONG_LONG:=8
PERL_SIZEOF_LONG_DOUBLE:=16
PERL_TYPE_i32:=int
PERL_TYPE_i64:=long
PERL_TYPE_QUAD:=long
PERL_QUAD_KIND:=2
PERL_UID_FORMAT:=u
endif


ifneq ($(PERL_VERSION),$(MICROPERL_VERSION))
$(DL_DIR)/$(PERL_SOURCE):
	$(WGET) -P $(DL_DIR) $(PERL_SITE)/$(PERL_SOURCE)
endif

$(PERL_DIR)/.unpacked: $(DL_DIR)/$(PERL_SOURCE)
	rm -rf $(BUILD_DIR)/tmp.perl
	mkdir -p $(BUILD_DIR)/tmp.perl
	$(PERL_CAT) $(DL_DIR)/$(PERL_SOURCE) | tar -C $(BUILD_DIR)/tmp.perl $(TAR_OPTIONS) -
	mv $(BUILD_DIR)/tmp.perl/perl-$(PERL_VERSION) $(@D)
	toolchain/patch-kernel.sh $(PERL_DIR) package/perl/ perl-$(PERL_VERSION)\*.patch
	touch $@

$(PERL_DIR)/.configured: $(PERL_DIR)/.unpacked
	# First we need to build a perl for the host..
	(cd $(PERL_DIR); ./Configure -de; \
	 $(MAKE1) CC="$(HOSTCC)" && \
	 mv perl hostperl && ln -sf hostperl miniperl; \
	)
	# ..then we can try to build it for the target. Must be some obscure
	# kind of feature.
	#
	# Unsurprisingly also it's configury sucks
	(cd $(PERL_DIR); rm -f config.sh Policy.sh; \
	 $(CONFIG_SHELL) ./Configure -deE \
		-DXXXusecrosscompile=yes_my_dear_and_apparently_you_could_impossibly_autodetect_this_fact \
		-DXXXcc="$(TARGET_CC)" \
		-DXXXcflags="$(TARGET_CFLAGS)" \
		-DXXXoptimize="$(TARGET_CFLAGS)" \
		-DXXXldflags="$(TARGET_LDFLAGS)" \
		-Dcf_by="Buildroot $(BR2_VERSION)" \
		-A define:osvers=$(LINUX_HEADERS_VERSION) \
		-Dinstallprefix="$(STAGING_DIR)" \
		-Dprefix=/usr \
		-Dvendorprefix=/usr \
		-Dsiteprefix=/usr \
		-Dprivlib=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		-Darchlib=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		-Dvendorlib=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		-Dvendorarch=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		-Dsitelib=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		-Dsitearch=$(STAGING_DIR)/$(PERL_MODS_DIR) \
		$(if $(BR2_ENABLE_SHARED),-Duseshrplib,-Uuseshrplib) \
		$(if $(BR2_PTHREADS_NONE),-Uusethreads -Uuseithreads,-Dusethreads -Duseithreads) \
		$(if $(BR2_LARGEFILE),-Duselargefiles -Dd_uselargefiles,-Uuselargefiles -Ud_uselargefiles -Ud_readdir64_r -Ud_off64_t) \
		-Ud_dosuid \
		-Ui_db \
		-Ui_ndbm \
		-Ui_gdbm \
		$(if $(BR2__UCLIBC_UCLIBC_HAS_SHADOW),-Di_shadow,-Ui_shadow) \
		$(if $(BR2__UCLIBC_UCLIBC_HAS_SYSLOG),-Di_syslog,-Ui_syslog) \
		-Ud_crypt_r \
		-Ud_getnetbyaddr_r \
		-Ud_getnetbyname_r \
		-Ud_getnetent_r \
		-Ud_eaccess \
		-Duseperlio \
		-Dman3ext=3pm \
		-Uafs \
		-Ud_csh \
		-Uusesfio \
		-Uusenm \
	)
	# despite specifying some desired settings above, this crap is broken
	# enough not to set the config.sh properly, not even trivia like osvers.
	# Fix this trash manually..
	$(SED) \
	  "s|^\(optimize\)=.*|\1=\'$(TARGET_CFLAGS)\'|" \
	  -e "s|^\(osversXXX\)=.*|\1=\'$(LINUX_HEADERS_VERSION)\'|" \
	  -e "s|^\(ar\)=.*|\1=\'$(TARGET_AR)\'|" \
	  -e "s|^\(full_ar\)=.*|\1=\'$(TARGET_AR)\'|" \
	  -e "s|^\(archname\)=.*|\1=\'$(REAL_GNU_TARGET_NAME)\'|" \
	  -e "s|^\(cc\)=.*|\1=\'$(TARGET_CC) $(TARGET_CC_FLAGS)\'|" \
	  -e "s|^\(ld\)=.*|\1=\'$(TARGET_LD) $(TARGET_LD_FLAGS)\'|" \
	  -e "s|^\(ldflags\)=.*|\1=\'$(TARGET_LDFLAGS)\'|" \
	  -e "s|/usr/local/|$(STAGING_DIR)/usr/|g" \
	  -e "s|^\(byteorder\)=.*|\1=\'$(PERL_BYTEORDER)\'|" \
	  -e "s|^\(alignbytes\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(longsize\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(ivsize\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(ptrsize\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(sizesize\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(uvsize\)=.*|\1=\'$(PERL_SIZEOF_LONG)\'|" \
	  -e "s|^\(longlongsize\)=.*|\1=\'$(PERL_SIZEOF_LONG_LONG)\'|" \
	  -e "s|^\(longdblsize\)=.*|\1=\'$(PERL_SIZEOF_LONG_DOUBLE)\'|" \
	  -e "s|^\(quadkind\)=.*|\1=\'$(PERL_QUAD_KIND)\'|" \
	  -e "s|^\(quadtype\)=.*|\1=\'$(PERL_TYPE_QUAD)\'|" \
	  -e "s|^\(uquadtype\)=.*|\1=\'unsigned $(PERL_TYPE_QUAD)\'|" \
	  -e "s|^\(i32type\)=.*|\1=\'$(PERL_TYPE_i32)\'|" \
	  -e "s|^\(u32type\)=.*|\1=\'unsigned $(PERL_TYPE_i32)\'|" \
	  -e "s|^\(i64type\)=.*|\1=\'$(PERL_TYPE_i64)\'|" \
	  -e "s|^\(u64type\)=.*|\1=\'unsigned $(PERL_TYPE_i64)\'|" \
	  -e "s|^\(d_u32align\)=.*|\1=\'$(if $(PERL_is_32),undef,define)\'|" \
	  -e "s|^\(nv_preserves_uv_bits\)=.*|\1=\'$(if $(PERL_is_32),32,53)\'|" \
	  -e "s|^\(use64bitall\)=.*|\1=\'$(if $(PERL_is_32),undef,define)\'|" \
	  -e "s|^\(use64bitint\)=.*|\1=\'$(if $(PERL_is_32),undef,define)\'|" \
	  -e "s|^\(selectminbits\)=.*|\1=\'$(if $(PERL_is_32),32,64)\'|" \
	  -e "s|^\(uidformat\)=.*|\1=\'\"$(PERL_UID_FORMAT)\"\'|" \
	  -e "s|^\(gidformat\)=.*|\1=\'\"$(PERL_UID_FORMAT)\"\'|" \
	  -e "s|^\(sPRIXU64\)=.*|\1=\'\"$(if $(PERL_is_32),LX,lX)\"\'|" \
	  -e "s|^\(sPRId64\)=.*|\1=\'\"$(if $(PERL_is_32),Ld,ld)\"\'|" \
	  -e "s|^\(sPRIi64\)=.*|\1=\'\"$(if $(PERL_is_32),Li,li)\"\'|" \
	  -e "s|^\(sPRIo64\)=.*|\1=\'\"$(if $(PERL_is_32),Lo,lo)\"\'|" \
	  -e "s|^\(sPRIu64\)=.*|\1=\'\"$(if $(PERL_is_32),Lu,lu)\"\'|" \
	  -e "s|^\(sPRIx64\)=.*|\1=\'\"$(if $(PERL_is_32),Lx,lx)\"\'|" \
	  -e "s|^\(sPRIEUldbl\)=.*|\1=\'\"$(if $(PERL_is_32),LE,lE)\"\'|" \
	  -e "s|^\(sPRIFUldbl\)=.*|\1=\'\"$(if $(PERL_is_32),LF,lF)\"\'|" \
	  -e "s|^\(sPRIGUldbl\)=.*|\1=\'\"$(if $(PERL_is_32),LG,lG)\"\'|" \
	  -e "s|^\(sPRIeldbl\)=.*|\1=\'\"$(if $(PERL_is_32),Le,le)\"\'|" \
	  -e "s|^\(sPRIfldbl\)=.*|\1=\'\"$(if $(PERL_is_32),Lf,lf)\"\'|" \
	  -e "s|^\(sPRIgldbl\)=.*|\1=\'\"$(if $(PERL_is_32),Lg,lg)\"\'|" \
	  -e "s|^\(sSCNfldbl\)=.*|\1=\'\"$(if $(PERL_is_32),Lf,lf)\"\'|" \
		$(PERL_DIR)/config.sh
ifneq ($(BR2_LARGEFILE),y)
	$(SED) \
	  "/cppsymbols/s/\(_LARGEFILE64_SOURCE\|_LARGEFILE_SOURCE\|__USE_LARGEFILE\|__USE_LARGEFILE64\)=[^[:space:]]*//" \
		$(PERL_DIR)/config.sh
endif
	cp -dpf $(PERL_DIR)/config.sh $(PERL_DIR)/config.sh.01
	(cd $(PERL_DIR); \
	 $(CONFIG_SHELL) ./Configure -f config.sh -reS \
	)
	touch $@

$(PERL_DIR)/perl: $(PERL_DIR)/.configured
	$(MAKE1) -C $(PERL_DIR)

$(TARGET_DIR)/usr/bin/perl: $(PERL_DIR)/perl
ifneq ($(PERL_MODS),)
	(cd $(PERL_DIR); \
	 for i in $(PERL_MODS); do \
	 	$(INSTALL) -D lib/$$i $(TARGET_DIR)/$(PERL_MODS_DIR)/$$i; \
	 done; \
	)
endif
	$(INSTALL) -D -m 0755 $(PERL_DIR)/perl $(TARGET_DIR)/usr/bin/perl
	(cd $(TARGET_DIR)/usr/bin; rm -f perl; ln -s perl perl;)

perl: $(TARGET_DIR)/usr/bin/perl

perl-source: $(DL_DIR)/$(PERL_SOURCE)

perl-clean:
	rm -rf $(TARGET_DIR)/usr/bin/perl \
		$(TARGET_DIR)/$(PERL_MODS_DIR) $(TARGET_DIR)/usr/bin/perl
	-rmdir $(TARGET_DIR)/usr/lib/perl$(PERL_MAJ)
	-$(MAKE) -C $(PERL_DIR) clean

perl-dirclean:
	rm -rf $(PERL_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(BR2_PACKAGE_PERL),y)
TARGETS+=perl
endif
