/* print the current environment to track eventually changed settings.
 *
 * Copyright (C) 2008 Bernhard Fischer
 *
 * Licensed under GPLv2 or later, see
 *   http://www.gnu.org/licenses/gpl.txt
 */
#include <unistd.h>
#include <stdio.h>
#include <string.h>
extern char **environ;

static const char skip[] =
	"_=\0SSH_CLIENT=\0SSH_TTY=\0SSH_CONNECTION=\0DISPLAY=\0"
	"VERBOSE=\0quiet=\0KBUILD_VERBOSE=\0Q=\0MAKEOVERRIDES=\0MAKEFLAGS=\0";
int main(void)
{
	char **envp;
	int ret = 0;
	for (envp = environ; *envp; envp++) {
		unsigned ok = 1;
		const char *str = skip;
		while (str) {
			size_t len = strlen(str);
			if (!len)
				break;
			if (!strncmp(str, envp[0], len)) {
				ok = 0;
				break;
			}
			str += ++len;
		}
		if (!ok)
			continue;
		if (puts(*envp) <= 0) {
			ret = 1;
			break;
		}
	}
	return ret;
}
